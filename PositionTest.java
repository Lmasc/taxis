import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import junit.framework.TestCase;

/**
 * Test implementation of the Position class.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class PositionTest extends TestCase
{
    /**
     * Default constructor for test class PositionTest
     */
    public PositionTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    protected void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    protected void tearDown()
    {
    }
    
    /**
     * Teste la méthode distance de la classe Position.
     */
    @Test
    public void testDistance()
    {
        boolean ok = true;
        int startX = 10, startY = 10;
        Position startPosition = new Position(startX, startY);
        
        // Calcule la distance de startPosition vers une autre
        // position autour d'elle. La distance devrait toujours
        // être égale à offset.
        int offset = 5;
        assertEquals(startPosition.distance(
            new Position(startX, startY + offset)), offset);
        assertEquals(startPosition.distance(
            new Position(startX + offset, startY)), offset);
        assertEquals(startPosition.distance(
            new Position(startX + 1, startY + offset)), offset);
        assertEquals(startPosition.distance(
            new Position(startX + offset, startY + 1)), offset);
        assertEquals(startPosition.distance(
            new Position(startX + offset, startY + offset)), offset);
        assertEquals(startPosition.distance(
            new Position(startX + offset - 1, startY + offset)), offset);
        assertEquals(startPosition.distance(
            new Position(startX + offset, startY + offset - 1)), offset);
    }
    
    /**
     * Exécute des tests pour la méthode nextPosition de la classe Position.
     */
    @Test
    public void testAdjacentPositions()
    {
        int startX = 10, startY = 10;
        Position startPosition = new Position(startX, startY);
        
        // Teste immédiatement adjacent.
        // (x, y) décalage pour cahque direction depuis (startX, startY).
        int[][] offsets = {
            { 0, 1, 0, 1, -1, 0, -1, 1, -1},
            { 0, 0, 1, 1, 0, -1, -1, -1, 1},
        };

        for(int i = 0; i < offsets[0].length; i++) {
            Position destination = new Position(startX + offsets[0][i],
                                       startY + offsets[1][i]);
            Position nextPosition = startPosition.nextPosition(destination);
            assertEquals(nextPosition.equals(destination), true);
        }
    }
    
    @Test
    public void testNonAdjacentPositions()
    {
        int startX = 10, startY = 10;
        Position startPosition = new Position(startX, startY);
        // (x, y) décalage pour cahque direction depuis (startX, startY).
        int[][] offsets = {
            { 0, 1, 0, 1, -1, 0, -1, 1, -1},
            { 0, 0, 1, 1, 0, -1, -1, -1, 1},
        };
        // Teste avec des destinations qui ne sont pas adjacentes.
        // Utilise différentes valeurs pur xDist at yDist pour
        // varier les tests.
        int xDist = 7;
        int yDist = 3;
        for(int i = 0; i < offsets[0].length; i++) {
            Position destination = new Position(startX + xDist * offsets[0][i],
                                       startY + yDist * offsets[1][i]);
            Position expectedNextPosition =
                        new Position(startX + offsets[0][i],
                                     startY + offsets[1][i]);
            Position nextPosition = startPosition.nextPosition(destination);            
            assertEquals(expectedNextPosition.equals(nextPosition), true);
        }
    }
}
